FROM python:3.8.2-alpine
ADD . /opt/
WORKDIR /opt/
EXPOSE 5000
RUN \
pip install --upgrade pip && \
pip3 install -r requirements.txt
ENV DB=mongodb://127.0.0.1:27017/tasks
CMD ["python","run.py"]